# Kimai Time Tracking

Make sure to change the owner of `plugins` directory to `www-data` before starting the container

```bash
sudo chown -R www-data:www-data plugins
```
