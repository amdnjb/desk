version: "3"

services:
  erpnext-nginx:
    image: frappe/erpnext-nginx:${ERPNEXT_VERSION}
    container_name: erpnext-nginx
    restart: unless-stopped
    ports:
      - "80:8080"
      - "81:81"
    environment:
      - FRAPPE_PY=erpnext-python
      - FRAPPE_PY_PORT=8000
      - FRAPPE_SOCKETIO=frappe-socketio
      - SOCKETIO_PORT=9000
      - SKIP_NGINX_TEMPLATE_GENERATION=${SKIP_NGINX_TEMPLATE_GENERATION}
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.erpnext-nginx.rule=Host(${SITES})"
      - "${ENTRYPOINT_LABEL}"
      - "${CERT_RESOLVER_LABEL}"
      - "traefik.http.services.erpnext-nginx.loadbalancer.server.port=8080"
    volumes:
      - sites-vol:/var/www/html/sites:rw
      - assets-vol:/assets:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/var/www/html/sites/common_site_config.json:ro
      - ${WORK_DIR}/healthy.conf:/etc/nginx/conf.d/healthy.conf:ro

  erpnext-python:
    image: frappe/erpnext-worker:${ERPNEXT_VERSION}
    container_name: erpnext-python
    restart: unless-stopped
    environment:
      - MARIADB_HOST=${MARIADB_HOST}
      - REDIS_CACHE=redis-cache:6379
      - REDIS_QUEUE=redis-queue:6379
      - REDIS_SOCKETIO=redis-socketio:6379
      - SOCKETIO_PORT=9000
      - AUTO_MIGRATE=1
      - WORKER_CLASS=${WORKER_CLASS}
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - assets-vol:/home/frappe/frappe-bench/sites/assets:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

  frappe-socketio:
    image: frappe/frappe-socketio:${FRAPPE_VERSION}
    container_name: frappe-socketio
    restart: unless-stopped
    depends_on:
      - redis-socketio
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - logs-vol:/home/frappe/frappe-bench/logs:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

  erpnext-worker-default:
    image: frappe/erpnext-worker:${ERPNEXT_VERSION}
    container_name: erpnext-worker-default
    restart: unless-stopped
    command: worker
    depends_on:
      - redis-queue
      - redis-cache
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - logs-vol:/home/frappe/frappe-bench/logs:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

  erpnext-worker-short:
    image: frappe/erpnext-worker:${ERPNEXT_VERSION}
    container_name: erpnext-worker-short
    restart: unless-stopped
    command: worker
    environment:
      - WORKER_TYPE=short
    depends_on:
      - redis-queue
      - redis-cache
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - logs-vol:/home/frappe/frappe-bench/logs:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

  erpnext-worker-long:
    image: frappe/erpnext-worker:${ERPNEXT_VERSION}
    container_name: erpnext-worker-long
    restart: unless-stopped
    command: worker
    environment:
      - WORKER_TYPE=long
    depends_on:
      - redis-queue
      - redis-cache
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

  erpnext-schedule:
    image: frappe/erpnext-worker:${ERPNEXT_VERSION}
    container_name: erpnext-schedule
    restart: unless-stopped
    command: schedule
    depends_on:
      - redis-queue
      - redis-cache
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - logs-vol:/home/frappe/frappe-bench/logs:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

  redis-cache:
    image: redis:latest
    container_name: redis-cache
    restart: unless-stopped
    volumes:
      - redis-cache-vol:/data

  redis-queue:
    image: redis:latest
    container_name: redis-queue
    restart: unless-stopped
    volumes:
      - redis-queue-vol:/data

  redis-socketio:
    image: redis:latest
    container_name: redis-socketio
    restart: unless-stopped
    volumes:
      - redis-socketio-vol:/data

  erpnext-site-creator:
    image: frappe/erpnext-worker:${ERPNEXT_VERSION}
    container_name: erpnext-site-creator
    restart: "no"
    command: new
    depends_on:
      - erpnext-python
    environment:
      - SITE_NAME=${SITE_NAME}
      - DB_ROOT_USER=${DB_ROOT_USER}
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
      - ADMIN_PASSWORD=${ADMIN_PASSWORD}
      - INSTALL_APPS=${INSTALL_APPS}
    volumes:
      - sites-vol:/home/frappe/frappe-bench/sites:rw
      - logs-vol:/home/frappe/frappe-bench/logs:rw
      - ${WORK_DIR}/data/sites/common_site_config.json:/home/frappe/frappe-bench/sites/common_site_config.json:ro

volumes:
  redis-cache-vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${WORK_DIR}/data/redis/cache
  redis-queue-vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${WORK_DIR}/data/redis/queue
  redis-socketio-vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${WORK_DIR}/data/redis/socketio
  assets-vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${WORK_DIR}/data/assets
  sites-vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${WORK_DIR}/data/sites
  logs-vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${WORK_DIR}/data/logs
