#!/bin/bash

# Flags
while getopts e:w:u:d:p:a: flag
do
  case "${flag}" in
    e) ENV=${OPTARG};;
    w) WORK_DIR=${OPTARG};;
    u) URL=${OPTARG};;
    d) DB=${OPTARG};;
    p) DB_PASS=${OPTARG};;
    a) ERP_PASS=${OPTARG};;
  esac
done

# AWS Configuration
if [ $ENV == "aws" ]
then
  cp $WORK_DIR/example-env $WORK_DIR/.env
  cp $WORK_DIR/example_common_site_config.json $WORK_DIR/common_site_config.json
  sed -i "s|WORK_DIR_PATH|$WORK_DIR|g" $WORK_DIR/.env
  sed -i "s|erpnext-mariadb|$DB|g" $WORK_DIR/common_site_config.json
  sed -i "s|erpnext-mariadb|$DB|g" $WORK_DIR/.env
  sed -i "s|DB_ROOT_PASSWORD|$DB_PASS|g" $WORK_DIR/.env
  sed -i "s|HOST.DOMAIN.TLD|$URL|g" $WORK_DIR/.env
  sed -i "s|ERPNEXT_ADMIN_PASSWORD|$ERP_PASS|g" $WORK_DIR/.env
  mkdir -p $WORK_DIR/data/redis/cache $WORK_DIR/data/redis/queue $WORK_DIR/data/redis/socketio $WORK_DIR/data/assets $WORK_DIR/data/sites $WORK_DIR/data/logs
  cp $WORK_DIR/common_site_config.json $WORK_DIR/data/sites/
  cp $WORK_DIR/docker-compose-$ENV.yml $WORK_DIR/docker-compose.yml

# Local Configuration
elif [ $ENV == "local" ]
then
  cp $WORK_DIR/example-env $WORK_DIR/.env
  sed -i "s|WORK_DIR_PATH|$WORK_DIR|g" $WORK_DIR/.env
  sed -i "s|DB_ROOT_PASSWORD|$DB_PASS|g" $WORK_DIR/.env
  sed -i "s|HOST.DOMAIN.TLD|$URL|g" $WORK_DIR/.env
  sed -i "s|ERPNEXT_ADMIN_PASSWORD|$ERP_PASS|g" $WORK_DIR/.env
  mkdir -p ${WORK_DIR}/data/mariadb $WORK_DIR/data/redis/cache $WORK_DIR/data/redis/queue $WORK_DIR/data/redis/socketio $WORK_DIR/data/assets $WORK_DIR/data/sites $WORK_DIR/data/logs
  cp $WORK_DIR/docker-compose-$ENV.yml $WORK_DIR/docker-compose.yml
fi

